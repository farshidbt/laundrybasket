package se.besharati.laundrybasket;

import java.util.Objects;

public class Household {

    private Integer id;

    public Household(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Household household = (Household) o;
        return Objects.equals(id, household.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
