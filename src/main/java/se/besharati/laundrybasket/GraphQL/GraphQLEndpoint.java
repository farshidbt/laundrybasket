package se.besharati.laundrybasket.GraphQL;

import com.coxautodev.graphql.tools.SchemaParser;
import graphql.schema.GraphQLSchema;
import graphql.servlet.SimpleGraphQLServlet;
import se.besharati.laundrybasket.Building;

import javax.servlet.annotation.WebServlet;

@WebServlet(urlPatterns = "/laundry")
public class GraphQLEndpoint extends SimpleGraphQLServlet {

    public GraphQLEndpoint() {
        super(buildSchema());
    }

    private static GraphQLSchema buildSchema() {
        Building building = new Building();

        return SchemaParser.newParser()
                .file("schema.graphqls")
                .resolvers(new Query(building))
                .build()
                .makeExecutableSchema();
    }
}
