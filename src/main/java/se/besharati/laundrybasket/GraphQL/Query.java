package se.besharati.laundrybasket.GraphQL;

import com.coxautodev.graphql.tools.GraphQLRootResolver;
import se.besharati.laundrybasket.Building;
import se.besharati.laundrybasket.Household;
import se.besharati.laundrybasket.LaundryRoom;

import java.util.List;

public class Query implements GraphQLRootResolver {

    private Building building;

    public Query(Building building) {
        this.building = building;
    }

    public List<Household> getHouseholds() {
        return this.building.getHouseholds();
    }

    public List<LaundryRoom> getLaundryRooms() {
        return this.building.getLaundryRooms();
    }
}
