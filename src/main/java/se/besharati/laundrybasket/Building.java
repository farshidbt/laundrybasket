package se.besharati.laundrybasket;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class Building {

    private final List<Household> houseHolds;
    private final List<LaundryRoom> laundryRooms;

    public Building() {
        this.houseHolds = IntStream.range(0, 20).boxed().map(Household::new).collect(Collectors.toList());
        this.laundryRooms = Stream.of(0, 1).map(LaundryRoom::new).collect(Collectors.toList());
    }

    public List<Household> getHouseholds() {
        return houseHolds;
    }

    public List<LaundryRoom> getLaundryRooms() {
        return laundryRooms;
    }

    public Household getHousehold(Integer homeId) {
        return houseHolds
                .stream()
                .filter(household -> household.getId().equals(homeId))
                .findFirst()
                .orElse(null);
    }
}
