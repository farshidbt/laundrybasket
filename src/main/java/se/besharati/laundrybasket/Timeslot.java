package se.besharati.laundrybasket;

import com.google.common.base.Preconditions;

import java.util.Objects;

public class Timeslot {
    private final Integer startHour;
    private Household household;

    public Timeslot(Integer startHour, Household household)  {
        Preconditions.checkArgument(startHour >= 7 && startHour < 22);

        this.startHour = startHour;
        this.household = household;
    }

    public Integer getStartHour() {
        return startHour;
    }

    public void book(Household household) {
        this.household = household;
    }

    public void cancel() {
        this.household = null;
    }

    public Household getHousehold() {
        return this.household;
    }

    @Override
    public String toString() {
        return "Timeslot{" +
                "startHour=" + startHour +
                ", household=" + household +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Timeslot timeslot = (Timeslot) o;
        return Objects.equals(startHour, timeslot.startHour) &&
                Objects.equals(household, timeslot.household);
    }

    @Override
    public int hashCode() {
        return Objects.hash(startHour, household);
    }
}
