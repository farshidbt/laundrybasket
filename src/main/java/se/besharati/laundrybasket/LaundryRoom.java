package se.besharati.laundrybasket;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class LaundryRoom {
    private Integer id;
    private List<Timeslot> timeslots = new ArrayList<>();

    public LaundryRoom(Integer id) {
        this.id = id;

        IntStream.range(7, 22).boxed().forEach(hour -> timeslots.add(new Timeslot(hour, null)));
    }

    public Integer getId() {
        return id;
    }

    public void bookTimes(Household household, List<Integer> hours) {
        timeslots.stream().filter(timeslot -> hours.contains(timeslot.getStartHour())).forEach(timeslot -> timeslot.book(household));
    }

    public void cancelTimes(List<Integer> hours) {
        this.timeslots.stream().filter(hours::contains).forEach(Timeslot::cancel);
    }

    public List<Timeslot> getAvailableTimes() {
        return timeslots.stream().filter(timeslot -> timeslot.getHousehold() == null).collect(Collectors.toList());
    }

    public List<Timeslot> getTimeslotsForHousehold(Household household) {
        return timeslots.stream()
                .filter(timeslot -> timeslot.getHousehold() != null && timeslot.getHousehold().equals(household))
                .collect(Collectors.toList());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        LaundryRoom that = (LaundryRoom) o;
        return Objects.equals(id, that.id);
    }

    public int hashCode() {
        return Objects.hash(id);
    }
}
