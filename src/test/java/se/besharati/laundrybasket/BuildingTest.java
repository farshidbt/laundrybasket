package se.besharati.laundrybasket;

import org.junit.Test;

import java.util.concurrent.ThreadLocalRandom;

import static org.junit.Assert.*;

public class BuildingTest {
    @Test
    public void getHousehold() {
        Building building = new Building();
        Household household = building.getHousehold(ThreadLocalRandom.current().nextInt(0, 19));

        assertNotNull(household);
    }

    @Test
    public void getSpecificHousehold() {
        Building building = new Building();
        Household household = building.getHousehold(17);

        assertEquals(new Integer(17), household.getId());
    }

    @Test
    public void getNullHousehold() {
        Building building = new Building();
        Household household = building.getHousehold(66);// Execute order 66

        assertNull(household);
    }
}