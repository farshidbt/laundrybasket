package se.besharati.laundrybasket;

import org.junit.Test;
import se.besharati.laundrybasket.Household;
import se.besharati.laundrybasket.LaundryRoom;
import se.besharati.laundrybasket.Timeslot;

import java.util.Collections;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static org.junit.Assert.assertEquals;

public class LaundryRoomTest {

    @Test
    public void bookTimeslot() {
        LaundryRoom laundryRoom = new LaundryRoom(1);
        Household household = new Household(ThreadLocalRandom.current().nextInt());
        Timeslot timeslot = new Timeslot(7, household);
        List<Timeslot> timeslots = Stream.of(timeslot).collect(Collectors.toList());

        laundryRoom.bookTimes(household, Collections.singletonList(7));
        List<Timeslot> actual = laundryRoom.getTimeslotsForHousehold(household);

        assertEquals(timeslots, actual);
    }

    @Test
    public void listAvailableTimeslots() {
        LaundryRoom laundryRoom = new LaundryRoom(1);
        Household household = new Household(ThreadLocalRandom.current().nextInt());

        List<Integer> hours = IntStream.rangeClosed(7, 12).boxed().collect(Collectors.toList());
        laundryRoom.bookTimes(household, hours);

        List<Timeslot> expected = IntStream.range(13, 22).boxed().map(hour -> new Timeslot(hour, null)).collect(Collectors.toList());
        List<Timeslot> actual = laundryRoom.getAvailableTimes();

        assertEquals(expected, actual);
    }

    @Test
    public void listBookedTimesForHousehold() {
        LaundryRoom laundryRoom = new LaundryRoom(1);
        Household household = new Household(ThreadLocalRandom.current().nextInt());

        List<Integer> hours = Stream.of(7, 12, 17, 21).collect(Collectors.toList());
        laundryRoom.bookTimes(household, hours);

        List<Timeslot> expected = Stream.of(7, 12, 17, 21).map(hour -> new Timeslot(hour, household)).collect(Collectors.toList());
        List<Timeslot> actual = laundryRoom.getTimeslotsForHousehold(household);

        assertEquals(expected, actual);
    }

    @Test
    public void cancelBookingForHousehold() {
        LaundryRoom laundryRoom = new LaundryRoom(1);
        Household household = new Household(ThreadLocalRandom.current().nextInt());

        laundryRoom.bookTimes(household, Stream.of(7, 12, 17, 21).collect(Collectors.toList()));
        laundryRoom.cancelTimes(Stream.of(7, 12, 17, 21).collect(Collectors.toList()));

        List<Timeslot> expected = Stream.of(7, 12, 17, 21).map(hour -> new Timeslot(hour, household)).collect(Collectors.toList());
        List<Timeslot> actual = laundryRoom.getTimeslotsForHousehold(household);

        assertEquals(expected, actual);
    }
}