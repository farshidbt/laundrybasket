package se.besharati.laundrybasket;

import org.junit.Test;
import se.besharati.laundrybasket.Household;
import se.besharati.laundrybasket.Timeslot;

import static org.junit.Assert.*;

public class TimeslotTest {

    @Test(expected = IllegalArgumentException.class)
    public void beforeSeven() {
        new Timeslot(6, null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void afterTen() {
        new Timeslot(23, null);
    }

    @Test
    public void bookATime() {
        Timeslot timeslot = new Timeslot(21, null);
        Household household = new Household(0);
        timeslot.book(household);

        assertNotNull(timeslot.getHousehold());
    }

    @Test
    public void cancelATime() {
        Timeslot timeslot = new Timeslot(21, new Household(0));

        timeslot.cancel();

        assertNull(timeslot.getHousehold());
    }
}